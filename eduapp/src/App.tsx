import React from "react";
import {BrowserRouter as Router,Routes,Route} from "react-router-dom";
import './App.css';
import {Signin} from "./component/signin/Signin";
import {Signup} from "./component/signup/Signup";

export const App: React.FC= () => {
  return (
    <div className="App">
      <Router>
        <Routes>
          <Route path="/" element={<Signin/>} />
          <Route path="signup" element={<Signup/>} />
        </Routes>
      </Router>
    </div>
  );
}