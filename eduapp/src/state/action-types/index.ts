import { ACTIONTYPE } from '../constants/actiontype/index';

interface user{
    id: number,
    name: string,
    email: string,
    password: string
}

interface AddUser{
    type : ACTIONTYPE.ADDUSER,
    payload: user
}

interface EditUser{
    type : ACTIONTYPE.EDITUSER,
    payload: user
}
 
interface DeleteUser{
    type : ACTIONTYPE.DELETEUSER,
    payload: number
}

export type Action = AddUser | EditUser | DeleteUser;

