import { Dispatch } from "react";
import { Action } from "../../action-types";
import { ACTIONTYPE } from "../../constants/actiontype";

interface user{
    id: number,
    name: string,
    email: string,
    password: string
}



const addUser = (user: user ) => {
    return(dispatch: Dispatch<Action>) => {
        dispatch({        
            type: ACTIONTYPE.ADDUSER,
            payload: user
        })
    }
} 

const editUser = (user: user) => {
    return(dispatch: Dispatch<Action>) => {
    dispatch({
        type: ACTIONTYPE.EDITUSER,
        payload: user
    })
}
} 

const deleteUser = (id: number) => {
    return(dispatch:Dispatch<Action>) => {
        dispatch({
            type: ACTIONTYPE.DELETEUSER,
            payload: id
        })
    }
} 



