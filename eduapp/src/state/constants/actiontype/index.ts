export enum ACTIONTYPE{
  ADDUSER = "AddUser",
  EDITUSER = "EditUser",
  DELETEUSER = "DeleteUser"
}
