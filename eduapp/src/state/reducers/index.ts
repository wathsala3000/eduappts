import { userReducer } from './userReducer';
import { combineReducers } from "redux";

const RootReducer = combineReducers({
    users: userReducer
})

export default RootReducer;