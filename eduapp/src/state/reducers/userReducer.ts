import { Action } from '../action-types/index';
import { RootState } from '../../store';
import { createSlice, PayloadAction  } from "@reduxjs/toolkit";
import { ACTIONTYPE } from '../constants/actiontype';

interface user{
    id: number,
    name: string,
    email: string,
    password: string
}

const initState : user = {
    id:1,
    name:"wathsala",
    email:"123@gmail",
    password:"password"
    };


export const userReducer = (state: user , action : Action) => {
    switch(action.type){
        case ACTIONTYPE.ADDUSER:
            return (state);

        case ACTIONTYPE.EDITUSER:
            return null;
        
        case ACTIONTYPE.DELETEUSER:
            return null;
        default:
            return null;
    }
}

export const users = (state: RootState) => state.users;
