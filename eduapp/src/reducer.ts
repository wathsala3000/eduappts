import { usersSlice } from './state/reducers/userReducer';
import { configureStore } from '@reduxjs/toolkit';

export const Reducer = configureStore({
    reducer : {
        user : usersSlice
    }
});